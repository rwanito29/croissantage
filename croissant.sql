-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : Dim 01 mars 2020 à 21:03
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `croissant`
--

-- --------------------------------------------------------

--
-- Structure de la table `class`
--

CREATE TABLE `class` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `class`
--

INSERT INTO `class` (`id`, `name`) VALUES
(1, 'IMR1'),
(2, 'IMR2'),
(3, 'IMR3');

-- --------------------------------------------------------

--
-- Structure de la table `croissantage`
--

CREATE TABLE `croissantage` (
  `id` int(11) NOT NULL,
  `idCed` int(11) NOT NULL COMMENT 'Croissanted person',
  `idCer` int(11) NOT NULL COMMENT 'person who croissanted, the croissanter',
  `dateC` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateCommand` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Deadline for pastry choice ',
  `deadline` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'max date to bring the command',
  `termine` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `croissantage`
--

INSERT INTO `croissantage` (`id`, `idCed`, `idCer`, `dateC`, `dateCommand`, `deadline`, `termine`) VALUES
(13, 4, 2, '2020-02-24 20:15:22', '2020-02-24 20:15:22', '2020-02-29 20:15:22', 0),
(18, 2, 2, '2020-02-26 17:49:07', '2020-02-26 17:49:07', '2020-03-04 17:49:07', 1),
(19, 4, 2, '2020-03-01 10:25:15', '2020-03-01 10:25:15', '2020-03-08 10:25:15', 0),
(20, 2, 2, '2020-03-01 10:27:35', '2020-03-03 10:27:35', '2020-03-08 10:27:35', 0);

-- --------------------------------------------------------

--
-- Structure de la table `currentcommand`
--

CREATE TABLE `currentcommand` (
  `id` int(11) NOT NULL,
  `idCroissantage` int(11) NOT NULL,
  `pastryType` int(11) NOT NULL,
  `idStudent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `currentcommand`
--

INSERT INTO `currentcommand` (`id`, `idCroissantage`, `pastryType`, `idStudent`) VALUES
(2, 20, 2, 2),
(6, 20, 1, 6),
(7, 20, 1, 7);

-- --------------------------------------------------------

--
-- Structure de la table `pastrytype`
--

CREATE TABLE `pastrytype` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `isAvailable` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pastrytype`
--

INSERT INTO `pastrytype` (`id`, `name`, `isAvailable`) VALUES
(1, 'Pain au Chocolat', 1),
(2, 'Croissant', 1),
(3, 'Chocolatine', 0),
(16, 'test', 0);

-- --------------------------------------------------------

--
-- Structure de la table `promo`
--

CREATE TABLE `promo` (
  `id` int(11) NOT NULL,
  `year` year(4) NOT NULL DEFAULT 1986,
  `idClass` int(11) NOT NULL,
  `idStudent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `promo`
--

INSERT INTO `promo` (`id`, `year`, `idClass`, `idStudent`) VALUES
(1, 1986, 1, 2),
(4, 2022, 1, 4),
(5, 2022, 1, 6),
(6, 0000, 1, 7);

-- --------------------------------------------------------

--
-- Structure de la table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `defaultPastry` int(11) DEFAULT 2,
  `rights` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `student`
--

INSERT INTO `student` (`id`, `login`, `alias`, `pwd`, `defaultPastry`, `rights`) VALUES
(2, 'min', 'plop', '31f7a65e315586ac198bd798b6629ce4903d0899476d5741a9f32e2e521b6a66', 2, 'admin'),
(4, 'test', 'test', '4ff17bc8ee5f240c792b8a41bfa2c58af726d83b925cf696af0c811627714c85', 2, 'Etudiant'),
(6, 'erwan', 'rwanito', '31f7a65e315586ac198bd798b6629ce4903d0899476d5741a9f32e2e521b6a66', 2, 'admin'),
(7, 'toto', 'toto', '31f7a65e315586ac198bd798b6629ce4903d0899476d5741a9f32e2e521b6a66', 2, 'admin');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `croissantage`
--
ALTER TABLE `croissantage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Croissanted` (`idCed`),
  ADD KEY `FK_Croissanter` (`idCer`);

--
-- Index pour la table `currentcommand`
--
ALTER TABLE `currentcommand`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `FK_UNIQUE_COMMANDE` (`idCroissantage`,`idStudent`) USING BTREE,
  ADD KEY `FK_CmdStudent` (`idStudent`),
  ADD KEY `FK_CmdPastryType` (`pastryType`),
  ADD KEY `FK_CmdCroissantage` (`idCroissantage`);

--
-- Index pour la table `pastrytype`
--
ALTER TABLE `pastrytype`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `promo`
--
ALTER TABLE `promo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Class` (`idClass`),
  ADD KEY `FK_StudentPromo` (`idStudent`);

--
-- Index pour la table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `U_alias` (`alias`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `class`
--
ALTER TABLE `class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `croissantage`
--
ALTER TABLE `croissantage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `currentcommand`
--
ALTER TABLE `currentcommand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `pastrytype`
--
ALTER TABLE `pastrytype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `promo`
--
ALTER TABLE `promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `croissantage`
--
ALTER TABLE `croissantage`
  ADD CONSTRAINT `FK_Croissanted` FOREIGN KEY (`idCed`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Croissanter` FOREIGN KEY (`idCer`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `currentcommand`
--
ALTER TABLE `currentcommand`
  ADD CONSTRAINT `FK_CmdCroissantage` FOREIGN KEY (`idCroissantage`) REFERENCES `croissantage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_CmdPastryType` FOREIGN KEY (`pastryType`) REFERENCES `pastrytype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_CmdStudent` FOREIGN KEY (`idStudent`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `promo`
--
ALTER TABLE `promo`
  ADD CONSTRAINT `FK_Class` FOREIGN KEY (`idClass`) REFERENCES `class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_StudentPromo` FOREIGN KEY (`idStudent`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
