# Croissantage

TP Noté de PHP
ENSSAT IMR1 - 2020

# Prérequis
PHP 7
MySQL ou MariaDB
Module rewrite pour apache

# Identifiants
login : min
mdp : toto

# Base de données
L'ensemble de la bdd est dans croissant.sql

# Autres
Utilisation de XAMPP pour avoir Apache, PHP et MariaDB.
