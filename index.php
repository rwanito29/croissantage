<?php

require_once(__DIR__."/lib/config.php");
require_once(__DIR__."/lib/login.php");
require_once(__DIR__."/lib/views.php");

$request = $_SERVER["REQUEST_URI"];
if(isset($_SERVER['REDIRECT_URL'])){
    $request = $_SERVER["REDIRECT_URL"];
}

$USER = isLogged();
// Vérification obligatoire => le login
if($request != "/login" && !$USER){
    // Si il n'est pas dans la page login AND il n'est pas connecté
    header("location: /login");
    exit();
}else if($request == "/login" && $USER){
    // Il est connecté mais toujours à la page login, alors redirection
    header("location: /index");
    exit();
}

// Router
switch ($request) {

    case '/' :
    case '' :
    case '/index' :
        $base_include = "index.php";
        require_once(__DIR__."/models/index.php");
        $PAGE = index_affichage($USER);
        break;

    case '/croissantage' :
        $base_include = "croissantage.php";

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            require_once(__DIR__."/models/croissantage.php");

            $PAGE = croissantage_post($USER, $_POST);
        }else{
            header("location: /");
            exit();
        }


        break;

    case '/about' :
        $base_include = "about.php";
        break;
        
    case '/login' : 
        
        $base_include = "login.php";

        require_once(__DIR__."/models/login.php");

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $PAGE = login_post($_POST);
        }else{
            $PAGE = login_affichage();            
        }

        break;

    case '/parametres' :
        $base_include = 'parametres.php';

        require_once(__DIR__."/models/parametres.php");

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $PAGE = parametres_post($_POST);
        }else{
            $PAGE = parametres_affichage();
        }
        
        break;

    case '/etudiants' :
        $base_include = "etudiants.php";

        
        require_once(__DIR__."/models/etudiants.php");

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $PAGE = etudiants_post($_POST);
        }else{
            $PAGE = etudiants_affichage();
        }

        break;

    case '/etudiant' :
        $base_include = "etudiant.php";
        
        require_once(__DIR__."/models/etudiant.php");

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $PAGE = etudiant_post($USER, $_POST);
        }else{
            $PAGE = etudiant_affichage();
        }

        break;

    case '/viennoiseries':

        $base_include = "viennoiseries.php";
        require_once(__DIR__."/models/viennoiseries.php");

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $PAGE = viennoiseries_post($_POST);
        }elseif(isset($_GET['togglePastry'])){
            $PAGE = viennoiseries_post($_GET);
        }else{
            $PAGE = viennoiseries_affichage();
        }

        break;

    case '/croissantages':

        $base_include = "croissantages.php";

        require_once(__DIR__."/models/croissantages.php");

        $PAGE = croissantages_affichage($USER);

        break;

    case '/deconnexion':

        delLogin();
        header("location: /login");
        exit();

    default:
        http_response_code(404);
        $base_include = "404.php";
        break;
}

require __DIR__ . '/views/base.php';