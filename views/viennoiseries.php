<div class="header">
    <h1>Gestion des viennoiseries</h1>
</div>

<div class="content">


   <?php if(isset($PAGE['messages'])): 
        foreach ($PAGE['messages'] as $message): ?>
        <p style="color:red;"><?=$message?></p>
    <?php endforeach; endif ?>

    <h2>Créer une viennoiserie</h2>

    <form class="pure-form pure-form-aligned" method="POST">
        <fieldset>

            <div class="pure-control-group">
                <label for="name">Nom de la viennoiserie</label>
                <input id="name" name="name" type="text" placeholder="Tarte aux pommes avec chocolat et caramel au beurre salé">
            </div>

            <div class="pure-controls">
                <input type="hidden" name="token" value="<?=$PAGE['token']?>">
                <button type="submit" class="pure-button pure-button-primary" name="creer">Créer !</button>
            </div>
        </fieldset>
    </form>

    <h2>Activation des viennoiseries</h2>

    <table class="pure-table pure-table-horizontal">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom</th>
                <th>Etat</th>
            </tr>
        </thead>

        <tbody>

            <?php foreach (PastryType::getTable() as $pastry): ?>

            <tr>
                <td><?=$pastry['id']?></td>
                <td><?=$pastry['name']?></td>
                <td><a href="?togglePastry=<?=$pastry['id']?>&token=<?=$PAGE['token']?>">
                    
                    <?php if($pastry['isAvailable']){
                        echo "Desactiver";
                    }else{
                        echo "Activer";
                    }
                ?>
                </a></td>
            </tr>

            <?php endforeach; ?>

        </tbody>
    </table>

</div>