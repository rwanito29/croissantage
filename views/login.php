<div class="header">
    <h1>Croissantage</h1>
    <h2>La page du croissantage !</h2>
</div>

<div class="content">

    <form class="pure-form" method="POST">
        <fieldset>
            <legend>Connexion :</legend>

            <?php afficherMessages("<p class=\"error\">", "</p>", $PAGE["messages"]); ?>

            <input type="text" placeholder="login" name="login" required="">
            <input type="password" placeholder="Mot de passe" name="password" required="">

            <input type="hidden" name="token" value="<?php echo $PAGE['token']; ?>">

            <button type="submit" class="pure-button pure-button-primary">Se connecter !</button>
        </fieldset>
    </form>

</div>