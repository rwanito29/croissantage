<div class="header">
    <h1>Paramètres</h1>
    <h2>Nouveau mot de passe, Changer son type de viennoiserie... !</h2>
</div>

<div class="content">

    <h3>Mon compte</h3>

    <form class="pure-form" method="POST">
        <fieldset>

            <legend>Modifier mon mot de passe</legend>

            <?php afficherMessages("<p class=\"error\">", "</p>", $PAGE["messages"]["formMDP"]); ?>

            <label for="ancienMDP">Ancien mot de passe</label>
            <input type="password" id="ancienMDP" name="ancienMDP" placeholder="Mot de passe" required="">

            <br/><br/>

            <label for="nvMDP">Ancien mot de passe</label>
            <input type="password" id="nvMDP" name="nvMDP" placeholder="Mot de passe" required="">

            <br/><br/>

            <input type="hidden" name="token" value="<?php echo $PAGE['token']; ?>">
            <button type="submit" class="pure-button pure-button-primary" name="submit" value="nvMDP">Changer mon mot de passe</button>
        </fieldset>
    </form>

    <form class="pure-form" method="POST">
        <fieldset>
            
            <legend>Modifier ma viennoiserie</legend>

            <?php foreach ($PAGE["tablePastryType"] as $type):
                if($type['id'] == $USER->defaultPastry AND !$type['isAvailable']): ?>
                    <p>Attention, vous utilisez une viennoiserie  (<?=$type['name']?>) indisponible, elle sera comptée comme une viennoiserie de base si rien n'est changé.</p>
            <?php endif; endforeach; ?>

            <?php afficherMessages("<p class=\"error\">", "</p>", $PAGE["messages"]["formPastryType"]); ?>

            <select name="pastryType">
                <?php


                foreach ($PAGE["tablePastryType"] as $type) {

                    $selected = "";

                    print_r($USER->defaultPastry);

                    if($type["isAvailable"]){

                        if($type["id"] == $USER->defaultPastry){
                            $selected = "selected";
                        }

                        echo "<option $selected value=\"".$type["id"]."\">".$type["name"]."</option>";
                    }
                }
                ?>
            </select>

            <br/><br/>

            <input type="hidden" name="token" value="<?php echo $PAGE['token']; ?>">
            <button type="submit" class="pure-button pure-button-primary" name="submit" value="pastryType">Changer ma viennoiserie</button>
        </fieldset>
    </form>

</div>