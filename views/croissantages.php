<div class="header">
    <h1>Croissantage</h1>
    <h2>La page du croissantage !</h2>
</div>

<div class="content">

    <h3>Stats et tableau</h3>

    <h4>Stats</h4>

    <p>Il y a eu <?=count($PAGE['tableau']['data'])?> croissantages !</p>

    <p>D'autres stats sont à venir</p>

    <h4>Tableau</h4>

    <table class="pure-table">
        <thead>
            <tr>
                <th>#</th>
                <th>De</th>
                <th>Vers</th>
                <th>Date creation</th>
                <th>Date Commande</th>
                <th>Deadline</th>
                <th>Terminé</th>
            </tr>
        </thead>

        <tbody>
        	<?php foreach ($PAGE['tableau']['data'] as $ligne) : ?>
            <tr>
                <td><?=$ligne['id']?></td>
                <td><?=$ligne['studentCer']?></td>
                <td><?=$ligne['studentCed']?></td>
                <td><?=$ligne['dateC']?></td>
                <td><?=$ligne['dateCommand']?></td>
                <td><?=$ligne['deadline']?></td>
                <td><?=$ligne['termine']?></td>
            </tr>

            <?php endforeach;?>
        </tbody>
    </table>

</div>