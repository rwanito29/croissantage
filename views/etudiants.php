<div class="header">
    <h1>Les étudiants</h1>
    <h2>Voir, modifier, supprimer les etudiants...</h2>
</div>

<div class="content">

    <h3>Gestion des etudiants</h3>

    <table class="pure-table">
        <thead>
            <tr>
                <th>#id</th>
                <th>login</th>
                <th>alias</th>
                <th>modifier</th>
            </tr>
        </thead>

        <tbody>

            <?php

            foreach (Etudiant::getListeEtudiant() as $etudiant): ?>

                <tr>
                    <td><?=$etudiant["id"]?></td>
                    <td><?=$etudiant["login"]?></td>
                    <td><?=$etudiant["alias"]?></td>
                    <td><a href="/etudiant?id=<?=$etudiant["id"]?>">Voir</a></td>
                </tr>

            <?php endforeach; ?>

        </tbody>
    </table>


    <?php if($USER->isAdmin()):?>

        <h4>Administration</h4>

        <form class="pure-form pure-form-stacked" method="POST">
            <fieldset>
                <legend>Créer un etudiant</legend>

                <?php if(isset($PAGE['messages'])):
                    foreach ($PAGE['messages'] as $message): ?>
                        <p style="color: red"><?=$message;?></p>
                <?php endforeach; endif;?>

                <div class="pure-g">
                    <div class="pure-u-1 pure-u-md-1-3">
                        <label for="login">Login</label>
                        <input id="login" class="pure-u-23-24" name="login" type="text" required>
                    </div>

                    <div class="pure-u-1 pure-u-md-1-3">
                        <label for="alias">Alias</label>
                        <input id="alias" class="pure-u-23-24" name="alias" type="text" required>
                    </div>

                    <div class="pure-u-1 pure-u-md-1-3">
                        <label for="mdp">Mot de passe</label>
                        <input id="mdp" class="pure-u-23-24" name="mdp" type="password" required>
                    </div>

                    <div class="pure-u-1 pure-u-md-1-3">
                        <label for="role">Role</label>
                        <input id="role" class="pure-u-23-24" name="role" type="text" required>
                    </div>

                    <div class="pure-u-1 pure-u-md-1-3">
                        <label for="promo">Promo</label>
                        <input id="promo" class="pure-u-23-24" name="promo" type="text" required>
                    </div>

                    <div class="pure-u-1 pure-u-md-1-3">
                        <label for="idClass">Classe</label>
                        <select id="idClass" name="idClass" class="pure-input-1-2">
                            <?php foreach (Classe::getListe()['data'] as $class):?>

                            <option value="<?=$class['id']?>"><?=$class['name']?></option>

                            <?php endforeach; ?>    
                        </select>
                    </div>

                </div>

                <br/>

                <input type="hidden" name="token" value="<?php echo $PAGE['token']; ?>">
                <button type="submit" class="pure-button pure-button-primary">Créer</button>
            </fieldset>
        </form>

    <?php endif; ?>
</div>