<div class="header">
    <h1>Gestion d'un etudiant</h1>
    <h2>Modifier/Supprimer</h2>
</div>

<div class="content">

    <h3>Etudiant</h3>

    <p>ID : <?=$PAGE["etudiant"]->id?>, login : <?=$PAGE["etudiant"]->login?>, alias : <?=$PAGE["etudiant"]->alias?></p>
    <p>Son role : <?=$PAGE["etudiant"]->role?></p>
    <p>PROMO : <?=$PAGE["etudiant"]->promo?>:<?=$PAGE["etudiant"]->anneePromo?></p>


    <?php if($USER->isAdmin()):?>

	    <h4>Administration</h4>

	    <?php if($USER->id != $PAGE["etudiant"]->id): ?>
		    <form method="POST" id="formSupprimer">
		    	<input type="hidden" name="token" value="<?php echo $PAGE['token']; ?>">
		    	<input type="hidden" name="id" value="<?=$PAGE["etudiant"]->id?>">
		    	<input type="submit" class="pure-button pure-button-primary" name="supprimer" value="Supprimer l'étudiant">
		    	<br/><br/>
		    </form>
		<?php else: ?>
    		<p>C'est vous-même, vous ne pouvez pas vous supprimer.</p>
	    <?php endif;?>

	    <form method="POST" class="pure-form pure-form-aligned">
	    	<label for="inputRole">Role</label>
	    	<input type="hidden" name="token" value="<?php echo $PAGE['token']; ?>">
	    	<input type="hidden" name="id" value="<?=$PAGE["etudiant"]->id?>">	  
	    	<input type="text" name="role" id="inputRole" value="<?=$PAGE["etudiant"]->role?>">
	    	<input type="submit" class="pure-button pure-button-primary" name="changerRole" value="Changer le rôle">
	    </form>

    <?php endif;?>
</div>

<script type="text/javascript">
	
	let formSupprimer = document.getElementById("formSupprimer");
	formSupprimer.onsubmit = function(ev){

		if(!confirm("Voulez-vous vraiment supprimer cet Etudiant ?"))
			ev.preventDefault();
	}

</script>