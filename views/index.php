<div class="header">
    <h1>Croissantage</h1>
    <h2>La page du croissantage !</h2>
</div>

<div class="content">

	<h3>Bonjour <?php echo $USER; ?></h3>

    <form class="pure-form" method="POST" action="/croissantage">
        <fieldset>
            <legend>Croissante ta victime :</legend>

            <input type="text" placeholder="Alias de la victime" name="victime">
            <input type="hidden" name="token" value="<?php echo $PAGE['token']; ?>">
            <button type="submit" class="pure-button pure-button-primary">Croissant !</button>
        </fieldset>
    </form>

    <hr />

    <h4>A propos de toi</h4>

    <?php if($PAGE['croissantage']['status']): ?>

    <form action="/croissantage" method="POST">

        <p>Tu as jusqu'au <?=$PAGE['croissantage']['data']['deadline']?> pour valider ton croissantage.</p>

        <?php if(date_parse($PAGE['croissantage']['data']['dateCommand']) < date_parse(time())):?>
        <p>Tu dois laisser encore du temps pour laisser la commande se faire. Les gens ont jusqu'au <?=$PAGE['croissantage']['data']['dateCommand']?> pour commander.</p>
       
        <?php else:?>

        <p>Tu dois commander :</p>
        <ul>
        <?php $nombreTotal = 0; foreach (Currentcommand::getCommande($PAGE['croissantage']['data']['id'])['data'] as $nom => $nombre): ?>
            <li><?=$nom?> : <?=$nombre?></li>
        <?php $nombreTotal+=$nombre; endforeach; ?>

            <li>Libre de ton choix : <?php echo (count(Etudiant::getListeEtudiantPromoClass($USER->promo, $USER->anneePromo)) - $nombreTotal); ?></li>
        </ul>

        <input type="hidden" name="idCroissantage" value="<?=$PAGE['croissantage']['data']['id']?>">
        <input type="hidden" name="token" value="<?php echo $PAGE['token']; ?>">
        <button type="submit" class="pure-button pure-button-primary">Commande distribuées !</button>

        <?php endif;?>
    </form>



    <?php endif; if(!$PAGE['croissantage']['status']){
        afficherMessages("<p>", "</p>", $PAGE['croissantage']['messages']);
    } ?>

    <hr />
    <h4>Les autres croissantages en cours</h4>
    <h5>Il est possible de modifier ton choix !</h5>

    <table class="pure-table">
        <thead>
            <tr>
                <th>#</th>
                <th>De</th>
                <th>Vers</th>
                <th>Date creation</th>
                <th>Date Commande</th>
                <th>Deadline</th>
                <th>Choix</th>
            </tr>
        </thead>

        <tbody>
            <?php $croissantageEnCours = Croissantage::getAllCroissatagesEnAttente($USER->id);
            foreach($croissantageEnCours['data'] as $ligne): 
            ?>
            <tr>
                <td><?=$ligne['id']?></td>
                <td><?=$ligne['studentCer']?></td>
                <td><?=$ligne['studentCed']?></td>
                <td><?=$ligne['dateC']?></td>
                <td><?=$ligne['dateCommand']?></td>
                <td><?=$ligne['deadline']?></td>
                <td>
                    <form method="POST" action="/croissantage">

                        <select name="pastryType">
                            <?php foreach ($PAGE['tablePastryDispo'] as $pastry): if($pastry['isAvailable']):
                                if($pastry['id'] == $USER->defaultPastry){
                                    $select = "selected";
                                }else{
                                    $select = "";
                                }
                            ?>
                            <option <?=$select?> value="<?=$pastry['id']?>"><?=$pastry['name']?></option>
                            <?php endif; endforeach;?>
                        </select>
                        
                        <input type="hidden" name="idCroissantage" value="<?=$ligne['id']?>">
                        <input type="hidden" name="token" value="<?php echo $PAGE['token']; ?>">
                        <button type="submit" class="pure-button pure-button-primary">Commander !</button>
                    </form>
                </td>
            </tr>

            <?php endforeach;?>
        </tbody>
    </table>


</div>