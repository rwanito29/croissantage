<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A layout example with a side menu that hides on mobile, just like the Pure website.">    
    <title>Je croissante</title>    
    <link rel="stylesheet" href="src/css/pure/pure-min.css">
    <link rel="stylesheet" href="src/css/layouts/side-menu.css">
    <link rel="stylesheet" type="text/css" href="src/css/default.css">
</head>
<body>

<div id="layout">
    <!-- Menu toggle -->
    <a href="#menu" id="menuLink" class="menu-link">
        <!-- Hamburger icon -->
        <span></span>
    </a>

    <div id="menu">
        <div class="pure-menu">
            <a class="pure-menu-heading" href="/">Je Croissante</a>

            <ul class="pure-menu-list">
                <li class="pure-menu-item"><a href="/" class="pure-menu-link">Accueil</a></li>

                <?php if($USER): ?>
                <li class="pure-menu-item"><a href="/croissantages" class="pure-menu-link">Voir tous les croissantages</a></li>
                <li class="pure-menu-item"><a href="/etudiants" class="pure-menu-link">Etudiants</a></li>
                <?php if($USER->isAdmin()): ?><li class="pure-menu-item"><a href="/viennoiseries" class="pure-menu-link">Viennoiseries</a></li><?php endif; ?>
                <li class="pure-menu-item"><a href="/parametres" class="pure-menu-link">Paramètres</a></li>
                <li class="pure-menu-item"><a href="/deconnexion" class="pure-menu-link">Deconnexion</a></li>
                <?php endif; ?>

                <li class="pure-menu-item"><a href="/about" class="pure-menu-link">About</a></li>
            </ul>
        </div>
    </div>

    <div id="main">
        <?php include($base_include); ?>
    </div>
</div>

<script src="src/js/ui.js"></script>

</body>
</html>