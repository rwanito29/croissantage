<?php 

require_once(__DIR__ ."/../lib/sessions.php");
require_once(__DIR__ ."/../lib/csrf.php");
require_once(__DIR__ ."/../lib/login.php");

function login_affichage(){

	// Generation de la variable utile
	$PAGE = [];

	// On a besoin d'un token
	$token = genererToken();
	setSession("token", $token);
	$PAGE["token"] = $token["token"];

	return $PAGE;
}

function login_post($form){

	$PAGE = [];

	/* 
		retourne :
		[
			"status": true or false
			"messages" ["Message 1", "Message2"]
		]

	*/

	// Verification du token
	if(isTokenValid(getSession("token"), $form["token"])){

		$PAGE = tryLogin($form['login'], $form['password'], true);
		if($PAGE["status"]){
			header("location: /index");
			exit();
		}

	}else{
		// La vérification a échoué
		// On relance un affichage normal

		$PAGE = affichage();
		$PAGE["messages"][] = "Session invalide. Recommencez.";
	}

	return $PAGE;
}