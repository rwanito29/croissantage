<?php 

require_once(__DIR__ ."/../lib/sessions.php");
require_once(__DIR__ ."/../lib/csrf.php");
require_once(__DIR__ ."/../lib/login.php");

function etudiant_affichage(){

	// Generation de la variable utile
	$PAGE = [];

	// On a besoin d'un token
	$token = genererToken();
	setSession("token", $token);
	$PAGE["token"] = $token["token"];

	// Gestion de l'etudiant
	$PAGE["etudiant"] = new Etudiant($_GET['id']);

	return $PAGE;

}

function etudiant_post($USER, $form){

	$PAGE = [];

	if(isTokenValid(getSession("token"), $form["token"])){

		if(isset($form['supprimer'])){

			Etudiant::supprimer($form['id']);
			header("location: /etudiants");
			exit();

		}elseif (isset($form['changerRole'])) {
			
			$PAGE = array_merge(Etudiant::updateRights($form['id'], $form['role']), $PAGE);

			if($USER->id == $form['id']){
				$USER->load();
			}
		}
	}

	$PAGE = array_merge(etudiant_affichage(), $PAGE);

	return $PAGE;
}