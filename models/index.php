<?php 

require_once(__DIR__ ."/../lib/csrf.php");
require_once(__DIR__ ."/../lib/login.php");
require_once(__DIR__ ."/../lib/croissantage.php");
require_once(__DIR__."/../lib/pastrytype.php");
require_once(__DIR__ ."/../lib/Currentcommand.php");

function index_affichage($USER){

	// Generation de la variable utile
	$PAGE = [];

	// On a besoin d'un token
	$token = genererToken();
	setSession("token", $token);
	$PAGE["token"] = $token["token"];

	// On regarde si un croissantage est en cours
	$PAGE['croissantage'] = Croissantage::getCroissantageEnCours($USER->id);
	$PAGE['tablePastryDispo'] = PastryType::getTable();

	return $PAGE;

}