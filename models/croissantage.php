<?php

require_once(__DIR__ ."/../lib/login.php");
require_once(__DIR__ ."/../lib/csrf.php");
require_once(__DIR__ ."/../lib/Croissantage.php");
require_once(__DIR__ ."/../lib/Currentcommand.php");

function croissantage_post($USER, $form){

	$PAGE = ["status" => false];

	if(isTokenValid(getSession("token"), $form["token"])){

		if(isset($form['victime'])){
			
			$id = Etudiant::getIdFromAlias($form['victime']);
			if($id['status']){

				$PAGE = Croissantage::croissanter($id['data'], $USER->id);
			}else{
				$PAGE["messages"] = $id['messages'];
			}
		}elseif (isset($form['idCroissantage']) AND isset($form['pastryType'])) {
			$PAGE = Currentcommand::insertCommand($form['idCroissantage'], $form['pastryType'], $USER->id);
		}
		elseif (isset($form['idCroissantage'])) {
			
			$PAGE = Croissantage::terminerCroissantage($form['idCroissantage']);
		}
	}

	return $PAGE;
}