<?php 

require_once(__DIR__ ."/../lib/sessions.php");
require_once(__DIR__ ."/../lib/csrf.php");
require_once(__DIR__."/../lib/pastrytype.php");
require_once(__DIR__."/../lib/login.php");

function parametres_affichage(){

	$PAGE = [];

	// Récuperation de la table des Pastry
	$PAGE["tablePastryType"] = PastryType::getTable();

	// On a besoin d'un token
	$token = genererToken();
	setSession("token", $token);
	$PAGE["token"] = $token["token"];

	return $PAGE;
}

function parametres_post($form){

	$PAGE = parametres_affichage();

	if(isTokenValid(getSession("token"), $form["token"])){

		if($form["submit"] == "nvMDP"){

			$PAGE = array_merge($PAGE, $GLOBALS['USER']->updateMdp($form["ancienMDP"], $form["nvMDP"]));
		}else if($form["submit"] == "pastryType"){

			$PAGE = array_merge($PAGE, $GLOBALS['USER']->updatePastryType($form["pastryType"]));
		}
	}

	return $PAGE;
}