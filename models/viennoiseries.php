<?php 

require_once(__DIR__ ."/../lib/sessions.php");
require_once(__DIR__ ."/../lib/csrf.php");
require_once(__DIR__."/../lib/pastrytype.php");
require_once(__DIR__."/../lib/login.php");

function viennoiseries_affichage(){

	$PAGE = [];

	// On a besoin d'un token
	$token = genererToken();
	setSession("token", $token);
	$PAGE["token"] = $token["token"];

	// Generation de la liste des viennoiseries

	return $PAGE;
}

function viennoiseries_post($form){

	$PAGE = [];

	if(isTokenValid(getSession("token"), $form["token"])){

		if(isset($form['creer'])){

			$PAGE = array_merge($PAGE, Pastrytype::creer($form['name']));
		}

		elseif(isset($form['togglePastry'])){

			$PAGE = array_merge($PAGE, Pastrytype::toggleAvailable($form['togglePastry']));
		}

	}

	$PAGE = array_merge($PAGE, viennoiseries_affichage());

	return $PAGE;
}