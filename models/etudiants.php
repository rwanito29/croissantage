<?php 

require_once(__DIR__ ."/../lib/sessions.php");
require_once(__DIR__ ."/../lib/csrf.php");
require_once(__DIR__ ."/../lib/login.php");

function etudiants_affichage(){

	// Generation de la variable utile
	$PAGE = [];

	// On a besoin d'un token
	$token = genererToken();
	setSession("token", $token);
	$PAGE["token"] = $token["token"];


	return $PAGE;

}

function etudiants_post($form){

	$PAGE = [];

	if(isTokenValid(getSession("token"), $form["token"])){

		$res = Etudiant::creerEtudiant($form["login"], $form["alias"], $form["mdp"], $form["role"], $form["promo"], $form['idClass']);

		$PAGE = array_merge($PAGE, $res, etudiants_affichage());
	}

	return $PAGE;
}