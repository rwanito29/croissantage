<?php

require_once(__DIR__ ."/../lib/bdd.php");

/**
 * 
 */
class Currentcommand
{
	
	public static function getCommande($idCroissantage){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{

			$requete = $bddPdo->prepare("SELECT * FROM `currentcommand` AS cc JOIN pastryType AS pt ON cc.pastrytype = pt.id WHERE cc.idCroissantage = :idCroissantage");
			$requete->bindValue(":idCroissantage", $idCroissantage);
			$requete->execute();
			
			if($data = $requete->fetchAll(PDO::FETCH_ASSOC)){

				$commande = [];
				foreach ($data as $commandeTmp) {
					if(isset($commande[$commandeTmp['name']])){
						$commande[$commandeTmp['name']] += 1;
					}else{
						$commande[$commandeTmp['name']] = 1;	
					}
					
				}

				$res = [
					"status" => true,
					"messages" => [],
					"data" => $commande
				];				
			}else{
				throw new Exception("Impossible de connaitre la raison de l'erreur.");
			}


		}catch(Exception $e){

			$res = [
				"status" => false,
				"messages" => ["Une erreur est survenue.", $e->getMessage()]
			];
			
		}finally{

			$maBdd->fermerBdd();
		}	

		return $res;	
	}

	public static function insertCommand($idCroissantage, $idPastry, $idStudent){


		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{

			$requete = $bddPdo->prepare("INSERT INTO currentcommand (idCroissantage, pastryType, idStudent) VALUES (:idCroissantage, :pastryType, :idStudent) ON DUPLICATE KEY UPDATE pastryType=:pastryType_update");
			$requete->bindValue(":idCroissantage", $idCroissantage);
			$requete->bindValue(":pastryType", $idPastry);
			$requete->bindValue(":pastryType_update", $idPastry);
			$requete->bindValue(":idStudent", $idStudent);

			if($requete->execute()){

				$res = [
					"status" => true,
					"messages" => ["Commandé !"]
				];				
			}else{
				throw new Exception("Impossible de connaitre la raison de l'erreur.");
			}


		}catch(Exception $e){

			$res = [
				"status" => false,
				"messages" => ["Une erreur est survenue.", $e->getMessage()]
			];
			
		}finally{

			$maBdd->fermerBdd();
		}	

		return $res;		
	}
}