<?php 

require_once(__DIR__ ."/../lib/sessions.php");
require_once(__DIR__ ."/../lib/bdd.php");
require_once(__DIR__ ."/../lib/Class.php");
require_once(__DIR__ ."/../lib/Promo.php");

define("INDEX_LOGIN", "login_utilisateur");

function isLogged(){

	$session = getSession(INDEX_LOGIN);
	$res = false;

	if(isset($session)){

		$res = unserialize($session);
		$res->load();
	}

	return $res;
}

function setLogin($etudiant){

	return setSession(INDEX_LOGIN, serialize($etudiant));
}

function delLogin(){
	return delSession(INDEX_LOGIN);
}

function tryLogin($login, $mdp, $boolSetLogin){
	// Enregistre dans les sessions login si boolSetLogin vaut true
	/*
		Retourne :
			[
				"status": "true OR false" // connecté ou pas
				"messages": ["erreur ou pas", "un deuxieme"]
			]
	*/

	$res = ["status" => true, "messages" => ["Connexion réussi !"]];

	$maBdd = new Bdd();
	$bddPdo = $maBdd->getBdd();

	try{
		$requete = $bddPdo->prepare("SELECT id FROM `student` WHERE login = :login AND pwd = :mdp");
		$requete->bindValue(":login", $login);
		$requete->bindValue(":mdp", hash("sha256", $mdp));
		$requete->execute();

		$resultats = $requete->fetch(PDO::FETCH_ASSOC);	

		if(!isset($resultats["id"])){
			throw new Exception("Login ou mot de passe incorrect !");
		}else if($boolSetLogin){

			setLogin(new Etudiant($resultats["id"]));
		}

	}catch(Exception $e){

		$res = [
			"status" 	=> false,
			"messages" 	=> [$e->getMessage()]
		];
	}finally{

		$maBdd->fermerBdd();
	}

	return $res;
}

/**
 * Etudiant
 */
class Etudiant{
	
	public $id;
	public $login;
	public $alias;
	public $defaultPastry;
	public $role;
	public $promo;
	public $anneePromo;

	function __construct($id){

		$this->id = $id;
		$this->load();
	}

	function __toString(){
		return $this->alias;
	}

	function isAdmin(){
		return strtolower($this->role) == "admin";
	}

	function load(){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{
			$requete = $bddPdo->prepare("SELECT student.id, student.alias, student.login, student.defaultPastry, student.rights, promo.year, class.name FROM student LEFT JOIN promo ON student.id = promo.idStudent LEFT JOIN class ON promo.idClass = class.id WHERE student.id = :id");

			$requete->bindValue(":id", $this->id);
			$requete->execute();

			$resultats = $requete->fetch(PDO::FETCH_ASSOC);

			if($resultats){

				$this->id = $resultats["id"];
				$this->alias = $resultats["alias"];
				$this->login = $resultats["login"];
				$this->defaultPastry = $resultats["defaultPastry"];
				$this->anneePromo = $resultats["year"];
				$this->role = $resultats["rights"];
				$this->promo = $resultats["name"];

				//print_r($resultats);

			}else{
				throw new Exception("Etudiant inconnu au bataillon.", $id);
			}

		}catch(Exception $e){

			throw $e;
		
		}finally{

			$maBdd->fermerBdd();
		}

	}

	function updateMdp($ancienMdp, $nvMDP){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();
		
		$res = [
			"status" => true,
			"messages" => [
				"formMDP" => ["Changement de mot de passe effectué avec succès !"]
			]
		];

		if(hash("sha256", $ancienMdp) == $this->getMdp()){

			try{
				$requete = $bddPdo->prepare("UPDATE student SET pwd = :nvMDP WHERE id = :id");
				$requete->bindValue(":nvMDP", hash("sha256", $nvMDP));
				$requete->bindValue(":id", $this->id);
				$requete->execute();

			}catch(Exception $e){

				throw $e;
			
			}finally{

				$maBdd->fermerBdd();
			}	


		}else{
			$res = [
				"status" => false,
				"messages" => [
					"formMDP" => ["L'ancien mot de passe ne correspond pas !"]
				]
			];
		}

		return $res;
	}

	private function getMdp(){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{
			$requete = $bddPdo->prepare("SELECT pwd FROM student WHERE id = :id");
			$requete->bindValue(":id", $this->id);
			$requete->execute();

			$resultats = $requete->fetch(PDO::FETCH_ASSOC);
			$res = $resultats['pwd'];

		}catch(Exception $e){

			$res = false;
		
		}finally{

			$maBdd->fermerBdd();
		}	

		return $res;	
	}

	public function updatePastryType($nvPastryType){

		$res = [
			"status" => true,
			"messages" => [
				"formPastryType" => ["Mise à jour avec succès du type de viennoiserie."]
			]
		];

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{
			$requete = $bddPdo->prepare("UPDATE student SET defaultPastry = :nvPastryType WHERE id = :id");
			$requete->bindValue(":nvPastryType", $nvPastryType);
			$requete->bindValue(":id", $this->id);
			$requete->execute();

			$this->load($this->id);

		}catch(PDOException $e){

			$res = [
				"status" => false,
				"messages" => [
					"formPastryType" => ["Erreur de la mise à jour du type de viennoiserie.", $e->getMessage()]
				]
			];
		
		}finally{

			$maBdd->fermerBdd();
		}

		return $res;
	}

	public static function getListeEtudiant(){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{
			$requete = $bddPdo->prepare("SELECT id, login, alias FROM student");
			$requete->execute();

			$res = $requete->fetchAll(PDO::FETCH_ASSOC);

		}catch(PDOException $e){

			$res = false;
		
		}finally{

			$maBdd->fermerBdd();
		}

		return $res;
	}
	public static function getListeEtudiantPromoClass($promo, $class){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{
			$requete = $bddPdo->prepare("SELECT student.id, student.login, student.defaultPastry, student.rights, promo.year, class.name FROM student JOIN promo ON promo.idStudent = student.id JOIN class ON class.id = promo.idClass");
			$requete->execute();

			$res = $requete->fetchAll(PDO::FETCH_ASSOC);

		}catch(PDOException $e){

			$res = false;
		
		}finally{

			$maBdd->fermerBdd();
		}

		return $res;
	}

	public static function supprimer($id){

		$res = [
			"status" => true,
			"messages" => ["Suppression avec succès !"]
		];

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{
			$requete = $bddPdo->prepare("DELETE FROM `student` WHERE id = :id");
			$requete->bindValue(":id", $id);
			$requete->execute();

		}catch(PDOException $e){

			$res = [
				"status" => false,
				"messages" => [$e->getMessage()]
			];
		
		}finally{

			$maBdd->fermerBdd();
		}

		return $res;
	}

	public static function updateRights($id, $right){

		$res = [
			"status" => true,
			"messages" => ["Mise à jour avec succès du droit."]
		];

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{
			$requete = $bddPdo->prepare("UPDATE student SET rights = :right WHERE id = :id");
			$requete->bindValue(":right", $right);
			$requete->bindValue(":id", $id);
			$requete->execute();

		}catch(PDOException $e){

			$res = [
				"status" => false,
				"messages" => ["Erreur lors de la mise à jour du droit.", $e->getMessage()]
			];
		
		}finally{

			$maBdd->fermerBdd();
		}

		return $res;		
	}

	public static function creerEtudiant($login, $alias, $mdp, $role, $promo, $idClass){

		$res = [
			"status" => true,
			"messages" => ["Ajout avec succès."]
		];

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{
			$requete = $bddPdo->prepare("INSERT INTO student (login, alias, pwd, rights) VALUES (:login, :alias, :pwd, :rights)");
			$requete->bindValue(":login", $login);
			$requete->bindValue(":alias", $alias);
			$requete->bindValue(":pwd", hash("sha256", $mdp));
			$requete->bindValue(":rights", $role);
			$requete->execute();

			$idStudent = $bddPdo->lastInsertId();

			$requete = $bddPdo->prepare("INSERT INTO promo (year, idClass, idStudent) VALUES (:year, :idClass, :idStudent)");
			$requete->bindValue(":year", $promo);
			$requete->bindValue(":idClass", $idClass);
			$requete->bindValue(":idStudent", $idStudent);
			$requete->execute();

		}catch(PDOException $e){

			$res = [
				"status" => false,
				"messages" => ["Erreur lors de l'ajout.", $e->getMessage()]
			];
		
		}finally{

			$maBdd->fermerBdd();
		}

		return $res;			
	}

	public static function getIdFromAlias($alias){

		$res = [
			"status" => true,
			"messages" => ["ID recupere."],
			"data" => []
		];

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{
			$requete = $bddPdo->prepare("SELECT id FROM student WHERE alias = :alias");
			$requete->bindValue(":alias", $alias);
			$requete->execute();

			$resultat = $requete->fetch(PDO::FETCH_ASSOC);
			if($resultat){
				$res["data"] = $resultat['id'];
			}else{
				$res = [
					"status" => false,
					"messages" => ["Utilisateur introuvable !"]
				];

			}

		}catch(PDOException $e){

			$res = [
				"status" => false,
				"messages" => ["Erreur lors de la recherche.", $e->getMessage()]
			];
		
		}finally{

			$maBdd->fermerBdd();
		}

		return $res;			
	}
}