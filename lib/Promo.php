<?php 

require_once(__DIR__ ."/../lib/bdd.php");

class Promo
{

	public static function getListe(){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{
			$requete = $bddPdo->prepare("SELECT * FROM promo");
			$requete->execute();

			$resultats = $requete->fetchAll(PDO::FETCH_ASSOC);	

			if($resultats){
				$res = [
					"status" 	=> true,
					"data" 	=> $resultats
				];
			}

		}catch(Exception $e){

			$res = [
				"status" 	=> false,
				"data" 	=> []
			];

		}finally{

			$maBdd->fermerBdd();
		}

		return $res;

	}


}