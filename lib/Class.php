<?php 

require_once(__DIR__ ."/../lib/bdd.php");

class Classe
{

	public $id;
	public $name;
	
	function __construct($id)
	{
		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		$requete = $bddPdo->prepare("SELECT id, name FROM `class` WHERE id = :id");
		$requete->bindValue(":id", $id);
		$requete->execute();

		$resultats = $requete->fetch(PDO::FETCH_ASSOC);	

		if($resultats){

			$this->id = $resultats["id"];
			$this->name = $resultats["name"];
		}

		$maBdd->fermerBdd();
	}

	public static function getListe(){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{
			$requete = $bddPdo->prepare("SELECT id, name FROM class");
			$requete->execute();

			$resultats = $requete->fetchAll(PDO::FETCH_ASSOC);	

			if($resultats){
				$res = [
					"status" 	=> true,
					"data" 	=> $resultats
				];
			}

		}catch(Exception $e){

			$res = [
				"status" 	=> false,
				"data" 	=> []
			];

		}finally{

			$maBdd->fermerBdd();
		}

		return $res;

	}


}