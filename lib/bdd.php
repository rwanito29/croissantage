<?php 


/**
 * Gestion de la base avec PDO
 */
class Bdd{

	private $bdd;
	
	function __construct(){
		
		$this->bdd = new PDO('mysql:host=localhost;dbname='.BDD_BASE, BDD_USER, BDD_MDP);
	    $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    $this->bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	}

	function getBdd(){

		return $this->bdd;
	}

	function fermerBdd(){

		$this->bdd = null;
	}
}
