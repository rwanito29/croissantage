<?php 

function ifSessions(){
	// Retourne vraie si session start est déjà fait.
	return session_status() != PHP_SESSION_NONE;
}

function startSession(){
	// Retourne vraie si changement, il y a.

	// Si session n'est pas démarrée alors on demarre
	if(!ifSessions()){
		session_start();

		return true;
	}

	return false;
}

function setSession($clef, $valeur){

	startSession();
	$_SESSION[$clef] = $valeur;

	return $clef;
}

function getSession($clef){
	
	startSession();

	$res = NULL;

	if(isset($_SESSION[$clef])){
		$res = $_SESSION[$clef];
	}

	return $res;
}

function delSession($clef){
	unset($_SESSION[$clef]);
}