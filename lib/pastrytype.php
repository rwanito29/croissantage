<?php

require_once(__DIR__ ."/../lib/bdd.php");

/**
 * PastryType
 */
class PastryType{

	public static function getTable(){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{
			$requete = $bddPdo->prepare("SELECT * FROM pastrytype");
			$requete->execute();

			$table = $requete->fetchAll(PDO::FETCH_ASSOC);

		}catch(Exception $e){

			throw $e;
		
		}finally{

			$maBdd->fermerBdd();
		}

		return $table;
	}

	public static function creer($nom){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		$res = [
			"status" 	=> true,
			"messages" 	=> ["Viennoiserie ajouté !"]
		];

		try{
			$requete = $bddPdo->prepare("INSERT INTO pastrytype (name) VALUES (:name)");
			$requete->bindValue(":name", $nom);
			$requete->execute();

		}catch(Exception $e){

			$res = [
				"status" 	=> false,
				"messages" 	=> [$e->getMessage()]
			];

		}finally{

			$maBdd->fermerBdd();
		}

		return $res;
	}

	public static function toggleAvailable($id){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		$res = [
			"status" 	=> true,
			"messages" 	=> ["Viennoiserie modifiée !"]
		];

		try{
			$requete = $bddPdo->prepare("UPDATE pastrytype SET isAvailable = IF(isAvailable=1, 0, 1) WHERE id = :id");
			$requete->bindValue(":id", $id);
			$requete->execute();

		}catch(Exception $e){

			$res = [
				"status" 	=> false,
				"messages" 	=> [$e->getMessage()]
			];

		}finally{

			$maBdd->fermerBdd();
		}

		return $res;

	}
}