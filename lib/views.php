<?php

function afficherMessages($balisesDebut, $balisesFin, &$array){

    if(isset($array)){
        foreach ($array as $message) {
            echo $balisesDebut.$message.$balisesFin;
        }

        unset($array);
    }
}