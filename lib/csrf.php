<?php

define("TIMEOUT_TOKEN", 10*60); // 10 minutes

function genererToken(){

	return 
	[
		"token" => bin2hex(random_bytes(32)),
		"time" => time()
	];
}


function isTokenValid($token, $postToken){
	// Prend un token [token, time] et compare avec une chaine token
	// Renvoie un message avec un status

	$res = [
		"status" => true,
		"messages" => ["Session valide."]
	];

	// Si token != postToken OU la diffèrence de temps est plus grande que TIMEOUT_TOKENm , alors invalide
	if($token["token"] != $postToken OR time()-$token["time"] > TIMEOUT_TOKEN){

		$res = [
			"status" => false,
			"messages" => ["Session invalide ou expirée."]
		];

	}

	return $res;
}
