<?php

require_once(__DIR__ ."/../lib/bdd.php");

define("DEADLINE_LIVRAISON_CROISSANTAGE", 7); // sept jours
define("DEADLINE_COMMANDE_CROISSANTAGE", 2); // sept jours

/**
 * 
 */
class Croissantage
{
	public static function terminerCroissantage($idCroissantage){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{

			$requete = $bddPdo->prepare("UPDATE croissantage SET termine = 1 WHERE id = :id");
			$requete->bindValue(":id", $idCroissantage);
			
			if($requete->execute()){
				$res = [
					"status" => true,
					"messages" => ["Croissantage terminé !"]
				];				
			}else{
				$res = [
					"status" => false,
					"messages" => ["Aucune action effectuée."]
				];
			}


		}catch(Exception $e){

			$res = [
				"status" => false,
				"messages" => ["Une erreur est survenue.", $e->getMessage()]
			];
			
		}finally{

			$maBdd->fermerBdd();
		}	

		return $res;	
	}

	public static function getCroissantageEnCours($idCroissante){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{

			$requete = $bddPdo->prepare("SELECT * FROM croissantage WHERE idCed = :idCed AND termine = 0");
			$requete->bindValue(":idCed", $idCroissante);
			
			if($requete->execute() AND $select = $requete->fetch(PDO::FETCH_ASSOC)){
				$res = [
					"status" => true,
					"messages" => [],
					"data" => $select
				];				
			}else{
				$res = [
					"status" => false,
					"messages" => ["Pas de croissantage en cours, ouf !"]
				];
			}


		}catch(Exception $e){

			$res = [
				"status" => false,
				"messages" => ["Une erreur est survenue.", $e->getMessage()]
			];
			
		}finally{

			$maBdd->fermerBdd();
		}	

		return $res;	
	}
	
	public static function croissanter($idCroissante, $idCroissanteur){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{

			$DEADLINE_LIVRAISON_CROISSANTAGE = DEADLINE_LIVRAISON_CROISSANTAGE;
			$DEADLINE_COMMANDE_CROISSANTAGE = DEADLINE_COMMANDE_CROISSANTAGE;

			// Verification si il est pas déjà croissanté
			$requete = $bddPdo->prepare("SELECT * FROM `croissantage` WHERE idCed = :idCed AND (NOW() BETWEEN dateC AND deadline AND termine = 0)");
			$requete->bindValue(":idCed", $idCroissante);
			$requete->execute();

			// Si vrai, on ne croisante pas
			if($requete->fetch(PDO::FETCH_ASSOC)){
				throw new Exception("Cet utilisateur est déjà croissanté ! Dommage.");
			}


			// Requete d'un croissantage
			$requete = $bddPdo->prepare("INSERT INTO croissantage (idCed, idCer, dateCommand, deadline) VALUES (:idCed, :idCer, NOW() + INTERVAL $DEADLINE_COMMANDE_CROISSANTAGE DAY, NOW() + INTERVAL $DEADLINE_LIVRAISON_CROISSANTAGE DAY)");
			$requete->bindValue(":idCed", $idCroissante);
			$requete->bindValue(":idCer", $idCroissanteur);
			
			if($requete->execute()){
				$res = [
					"status" => true,
					"messages" => ["Victime croissantée avec succès ! "]
				];				
			}else{
				throw new Exception("Impossible de connaitre la raison de l'erreur.");
			}


		}catch(Exception $e){

			$res = [
				"status" => false,
				"messages" => ["Une erreur est survenue.", $e->getMessage()]
			];
			
		}finally{

			$maBdd->fermerBdd();
		}	

		return $res;	
	}

	public static function getAllCroissatages($enCours = false){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		if($enCours){
			$SQL_ENCOURS = "croissantage.termine = 0";
		}else{
			$SQL_ENCOURS = "1";
		}

		try{

			// Verification si il est pas déjà croissanté
			$requete = $bddPdo->prepare("SELECT croissantage.id, croissantage.dateC, croissantage.dateCommand, croissantage.deadline, croissantage.termine, studentCed.alias AS studentCed, studentCer.alias AS studentCer FROM `croissantage` JOIN student AS studentCed ON studentCed.id = croissantage.idCed JOIN student AS studentCer ON studentCer.id = croissantage.idCer WHERE $SQL_ENCOURS ORDER BY croissantage.deadline");
			$requete->execute();

			// Si vrai, on ne croisante pas
			if($data = $requete->fetchAll(PDO::FETCH_ASSOC)){
				$res = [
					"status" => true,
					"messages" => [],
					"data" => $data
				];
			}else{
				throw new Exception("Aucune croissantage pour l'instant.");
			}

		}catch(Exception $e){

			$res = [
				"status" => false,
				"messages" => ["Une erreur est survenue.", $e->getMessage()],
				"data" => []
			];
			
		}finally{

			$maBdd->fermerBdd();
		}	

		return $res;	
	}

	public static function getAllCroissatagesEnAttente(){

		$maBdd = new Bdd();
		$bddPdo = $maBdd->getBdd();

		try{

			$requete = $bddPdo->prepare("SELECT croissantage.id, croissantage.dateC, croissantage.dateCommand, croissantage.deadline, croissantage.termine, studentCed.alias AS studentCed, studentCer.alias AS studentCer FROM `croissantage` JOIN student AS studentCed ON studentCed.id = croissantage.idCed JOIN student AS studentCer ON studentCer.id = croissantage.idCer WHERE croissantage.termine = 0 AND NOW() < croissantage.dateCommand ORDER BY croissantage.deadline");
			$requete->execute();

			// Si vrai, on ne croisante pas
			if($data = $requete->fetchAll(PDO::FETCH_ASSOC)){
				$res = [
					"status" => true,
					"messages" => [],
					"data" => $data
				];
			}else{
				throw new Exception("Aucune croissantage pour l'instant.");
			}

		}catch(Exception $e){

			$res = [
				"status" => false,
				"messages" => ["Une erreur est survenue.", $e->getMessage()],
				"data" => []
			];
			
		}finally{

			$maBdd->fermerBdd();
		}	

		return $res;	
	}
}